#!/usr/bin/python

import socket
import random
import re
import pickle
import datetime

def getResp(soc) :
 msg = soc.recv(4096)
 print msg
 return msg

def sendReq(soc, msg) :
 msg = msg+'\r\n'
 print msg
 soc.send(msg)
 return msg

def secret_number(soc) :
 min = 1500
 max = 9000
 attempts = 20
 patternS = re.compile("nope. Try something smaller", re.IGNORECASE)
 patternB = re.compile("nope. Try something bigger", re.IGNORECASE)
 
 for attempt in range(attempts) :
  rand_number = random.randint(min, max)
  reply = str(rand_number)+'\r\n'
  print reply
  soc.send(reply)
  response = getResp(soc)
  if patternS.search(response) :
   max = rand_number
  elif patternB.search(response) :
   min = rand_number
  else :
   break
 return response

def sendtextasciiart(soc, asciiart) :
 num1 = []
 num2 = []
 num3 = []
 eachline = asciiart.split('\n')
 for each in eachline :
  hash1 = 0
  hash2 = 0
  hash3 = 0
  for i in each[0:7] :
   if(i == '#') :
    hash1 = hash1 + 1
  for i in each[16:30] :
   if(i == '#') :
    hash2 = hash2 + 1
  for i in each[30:39] :
   if(i == '#') :
    hash3 = hash3 + 1
  num1.append(hash1)
  num2.append(hash2)
  num3.append(hash3)
 asciiartnum = compare(num1, num2, num3)
 return asciiartnum

def compare(num1, num2, num3) :
 inp = [num1, num2, num3]
 complist = [[3, 2, 3, 3, 3, 2, 3, 0, 0, 0], [1, 2, 2, 1, 1, 1, 5, 0, 0, 0], [5, 2, 1, 5, 1, 1, 7, 0, 0, 0], [5, 2, 1, 5, 1, 2, 5, 0, 0, 0], [1, 2, 2, 7, 1, 1, 1, 0, 0, 0], [7, 1, 1, 5, 1, 2, 5, 0, 0, 0], [5, 2, 1, 6, 2, 2, 5, 0, 0, 0], [7, 2, 1, 1, 1, 1, 1, 0, 0, 0], [5, 2, 2, 5, 2, 2, 5, 0, 0, 0], [5, 2, 2, 6, 1, 2, 5, 0, 0, 0]]
 result = []
 for lists in inp :
  for eachlist in complist :
   if (cmp(eachlist, lists) == 0) :
    result.append(complist.index(eachlist))
    break
 result = map(str, result)
 result = ''.join(result)
 return result

def unpickle(pickled) :
 res = pickled.split('\n')[1:9]
 value = '\n'.join(res)
 unpickle = pickle.loads(value)
 micro = unpickle.microsecond
 return micro

def weekday(dateline) :
 justdate = re.compile('\d{2} [A-Z][a-z]{2} \d{2}')
 value = justdate.findall(dateline)[0]
 newdate = datetime.datetime.strptime(value, '%d %b %y').strftime('%A')
 return newdate


soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 9999
soc.connect(('127.0.0.1', port))
getResp(soc)
getResp(soc)
sendReq(soc,'newbie')
getResp(soc)
sendReq(soc,'b518d6cc749022a5f25ac327a13df99d')
getResp(soc)
result = secret_number(soc)

pattern = re.compile("BRAVO", re.IGNORECASE)
if pattern.search(result) :
 asciiart = getResp(soc)
 number = sendtextasciiart(soc, asciiart)
 sendReq(soc, number)
 getResp(soc)
 pickled = getResp(soc)
 micro = unpickle(pickled)
 sendReq(soc, str(micro))
 getResp(soc)
 dateline = getResp(soc)
 theday = weekday(dateline)
 sendReq(soc, theday)
 almost = getResp(soc)
 secret = almost.split()
 print secret[5]

soc.close()

