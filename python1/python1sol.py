#!/usr/bin/python

import sys

def getmaxnum(eachline) : 
 max = 0
 for i in eachline.split(' ') :
  if(i[0].isdigit()) :
   #if(int(i) > max) :
   max = int(i)
   break
 return max
  
listoflines = sys.stdin.readlines()
sortedlist = sorted(listoflines, key=getmaxnum, reverse=True)

n = int(sys.argv[1])
previous = 0
counter = 0
for ele in sortedlist :
 current = getmaxnum(ele)
 if(previous != current and counter >= n) :
  break
 counter = counter + 1
 previous = current
 print ele.strip()
